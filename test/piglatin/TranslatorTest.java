package piglatin;

import static org.junit.Assert.*;

import org.junit.Test;

public class TranslatorTest {

	@Test
	public void testInputPhrase() {
		
		Translator translator = new Translator("hello world");
		assertEquals("hello world", translator.getPhrase());
	}
	
	@Test
	public void testVoidPhrase() {
		
		Translator translator = new Translator("");
		assertEquals(Translator.NIL, translator.translate());
	}

	@Test
	public void shouldAppendNay() {
	
		Translator translator = new Translator("Any");
		assertEquals("Anynay", translator.translate());
	}
	
	@Test
	public void shouldAppendYay() {
		
		Translator translator = new Translator("Apple");
		assertEquals("Appleyay", translator.translate());
	}
	
	@Test
	public void shouldAppendAy() {
		
		Translator translator = new Translator("Ask");
		assertEquals("Askay", translator.translate());
	}
	
	@Test
	public void moveFirstLetterAndAppendAy() {
		
		Translator translator = new Translator("Hello");
		assertEquals("elloHay", translator.translate());
	}
	
	@Test
	public void moveFirstConsonantsAndAppendAy() {
		
		Translator translator = new Translator("Known");
		assertEquals("ownKnay", translator.translate());
	}
	
	@Test
	public void moveFirstConsonantsAndAppendAyPartTwo() {
		
		Translator translator = new Translator("Gcvl");
		assertEquals("Gcvlay", translator.translate());
	}
	
	@Test
	public void testMoreWords() {
		Translator translator = new Translator("Hello World");
		assertEquals("elloHay orldWay", translator.translate());
	}
	
	@Test
	public void punctuationShouldBeRespected() { 
		
		Translator translator = new Translator("Hello World!");
		assertEquals("elloHay orldWay!", translator.translate());
	}
	
	@Test
	public void testHardPhrase() {
		
		Translator translator = new Translator("Tua mamma. puzza assai ");
		assertEquals("uaTay ammamay. uzzapay assaiyay ", translator.translate());
		
	}
}
