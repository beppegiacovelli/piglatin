package piglatin;

public class Translator {

	
	public static final String NIL = "nil";
	private String phrase;
	
	public Translator(String phrase) {
		
		this.phrase = phrase;
	
	}

	public String getPhrase() {
		
		return phrase;
	}

	public String translate() {
		if (hasMoreWordsOrPunctuation())
			return translatePhrase();
		
		if (startsWithVowel()) {
			
			if (phrase.endsWith("y") || phrase.endsWith("Y"))
				return phrase + "nay";
			
			else if(endsWithVowel())
				return phrase + "yay";
			
			else 
				return phrase + "ay";
		}
		else if (phrase.length() != 0){
			
			String string = phrase;
			StringBuilder finalString = new StringBuilder();
			
			while (!startsWithVowel(string)) {
				Character first = string.charAt(0);
				finalString.append(first);
				
				string = string.substring(1);
				
				if (string.length() == 0)
					break;
				
			}
			
			return string + finalString + "ay";
			
		}
			
		return NIL;
	}
	
	private String translatePhrase() {

		char[] letters = phrase.toCharArray();
		StringBuilder string = new StringBuilder();
		String startingPhrase = phrase;
	
		StringBuilder finalString = new StringBuilder();
		
		for ( int i = 0; i<letters.length; i++) {
			
			if (letters[i] != ' ' && letters[i] != '-' && !(isPunctuation(letters[i])) )
				string.append(letters[i]);
			
			else {
				
				if (string.length() > 1) {
					phrase = string.toString();
					finalString.append(translate());
				}
				finalString.append(letters[i]);
				string.setLength(0);
			}
		}
		
		String finalWord = "";
		
		if (string.length() != 0) {
			phrase = string.toString();
			finalWord = translate();
		}
		
		phrase = startingPhrase;
		
		return finalString.toString() + finalWord;
		
	}

	private boolean startsWithVowel() {
		return (phrase.startsWith("a") || phrase.startsWith("e") || phrase.startsWith("i") || phrase.startsWith("o") || phrase.startsWith("u")  ||
			phrase.startsWith("A") || phrase.startsWith("E") || phrase.startsWith("I") || phrase.startsWith("O") || phrase.startsWith("U"));
	}
	
	private boolean startsWithVowel(String string) {
		return (string.startsWith("a") || string.startsWith("e") || string.startsWith("i") || string.startsWith("o") || string.startsWith("u")  ||
			string.startsWith("A") || string.startsWith("E") || string.startsWith("I") || string.startsWith("O") || string.startsWith("U"));
	}
	
	private boolean endsWithVowel() {
		if(phrase.endsWith("a") || phrase.endsWith("e") || phrase.endsWith("i") || phrase.endsWith("o") || phrase.endsWith("u") ||
			phrase.endsWith("A") || phrase.endsWith("E") || phrase.endsWith("I") || phrase.endsWith("O") || phrase.endsWith("U"))
			return true;
		
		return false;
	}
	
	private boolean hasMoreWordsOrPunctuation() {
		return (phrase.contains(" ") || phrase.contains("-") || phrase.contains(".") || phrase.contains(",") || phrase.contains(":") || phrase.contains(";") || phrase.contains("?") 
				|| phrase.contains("!") || phrase.contains("(") || phrase.contains(")") || phrase.contains("'"));
	}
	
	private boolean isPunctuation(char c) {
		return (c == '.' || c == ',' || c == ';' || c == ',' || c == '?' || c == '!' || c == '(' || c == ')' || c == '\'');
	}

}
